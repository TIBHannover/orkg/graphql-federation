
const fetch = require('node-fetch');
const { mergeSchemas } = require('graphql-tools');
const getORKGSchema = require('./ORKG.js');
const getDataCiteSchema = require('./DataCite.js');
const { combinedTypeDefs } = require('./combinedSchema/combinedSchema');
const { combinedResolvers } = require('./resolvers/resolvers');
const { ApolloServer } = require('apollo-server');
const { exampleQueries } = require('./ExampleQueries/AllQueries');

const PORT = process.env.PORT || 4001;

const extendSchema = async () => {

  const ORKGSchema = await getORKGSchema();
  const DataCiteSchema = await getDataCiteSchema();

  const newSchema = mergeSchemas({
    schemas: [
      ORKGSchema,
      DataCiteSchema,
      combinedTypeDefs
    ],
    resolvers: combinedResolvers
  });
  return newSchema;
};

const startServer = async () => {
  const schema = await extendSchema();
  const server = new ApolloServer({
    schema,
	  introspection: true,
  playground: {
    settings: {
      'editor.theme': 'light',
    },
    //tabs: exampleQueries
  }
});

  server.listen(PORT);
};

try {
  startServer();
} catch (e) {
  console.error(e);
}

process.on('SIGTERM', () => {
  server.close(() => {
    console.log('Process terminated')
  });
});
