const {query1} = require('./query1');
const {query2} = require('./query2');
const {query3} = require('./query3');

var ENDPOINT = '';
if(process.env.NODE_ENV === 'production') {
  ENDPOINT = 'https://www.orkg.org/orkg/graphql-federated';
} else {
  ENDPOINT = 'http://localhost:4001/';
}

const exampleQueries = [
  {
    name: 'COVID-19 meta(data) analysis',
    endpoint: ENDPOINT,
    query: query1
  },

  {
    name: 'finding related artefacts',
    endpoint: ENDPOINT,
    query: query2
  },

  {
    name: 'COVID-19 co-authors network',
    endpoint: ENDPOINT,
    query: query3
  }
];

module.exports = {exampleQueries};
