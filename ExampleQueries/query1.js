
const query1 = `# Federated query to fetch number of citations from DataCite and fetching related papers from ORKG
{
  #DataCite query
  work(id: "https://doi.org/10.1101/2020.03.08.20030643") {
    id
    titles {
      title
    }
    citations {
      totalCount
      nodes {
        titles {
          title
        }
        publisher
        #ORKG query
        comparison {
          label
          doi {
            label
          }
          # Retrieving countries of Asian (AS) continent and filtering them
          relatedPapers(property: "location", value: "AS") {
            id {
              label
            }
            authors {
              label
            }
            #DataCite query
            work {
              publisher
            }
            #ORKG query
            contributions {
              contribution_details {
                property {
                  label
                }
                label
                contribution_details {
                  property {
                    label
                  }
                  label
                  contribution_details {
                    property {
                      label
                    }
                    label
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
`;

module.exports = {query1};
