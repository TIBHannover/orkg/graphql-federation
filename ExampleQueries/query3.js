
const query3 = `# extract papers for a researcher who has published COVID-19 papers and reported 95% confidence interval. 
{
  person(id: "https://orcid.org/0000-0001-8722-6149") {
    id
    name
    works {
      nodes {
        id
        Paper(field: ["covid-19", "coronavirus"]) {
          label
          details(value: "Confidence interval") {
            label
            details {
              label
            }
          }
        }
      }
    }
  }
}
`;

module.exports = {query3};
