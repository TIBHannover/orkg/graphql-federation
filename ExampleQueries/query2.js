
const query2 = `# Find related comparisons and dissertations for a particular research problem
{
  # ORKG query
  Problem(label: "Global Mean Sea Level Rise Projections") {
    label
    relatedComparisons {
      label
    }
    # DataCite query
    dissertations {
      nodes {
        id
        publicationYear
        publisher
        creators {
          id
          name
        }
        titles {
          title
        }
      }
    }
  }
}
`;

module.exports = {query2};
