var rp = require("request-promise");
const {
  findCountriesByContinent,
  findCitiesByCountry,
} = require("../GeoNames/geoConnector");

async function getComparisonData(relatedPapers) {
  let res = [];
  let resource_ids = [];
  let ids = "";
  let comparisonData = [];
  //console.log(relatedPapers[0]['authors']);
  for (let i = 0; i < relatedPapers.length; i++) {
    let contributions = relatedPapers[i].contributions;

    contributions.map((c) => {
      resource_ids.push(c["resource_id"]);
    });
  }
  const url = `https://www.orkg.org/orkg/simcomp/compare/?contributions=${resource_ids.join()}&response_hash=8189e27e4d9c597f2784d033f1ae0437&save_response=false&type=merge`;
  const options = {
    uri: url,
    json: true,
  };

  await rp(options)
    .then((data) => {
      let comparisonContributions = data["contributions"];
      let properties = data["properties"];
      let values = data["data"];
      let cont = [];
      comparisonContributions.map((c) => {
        comparisonData[c["id"]] = [];
        Object.keys(values).map(function (key, index) {
          for (let k = 0; k < values[key].length; k++) {
            if (
              values[key][k][0]["path"] &&
              values[key][k][0]["path"].indexOf(c["id"]) !== -1
            ) {
              let property = properties.find((p) => p["id"] === key);
              comparisonData[c["id"]].push({
                property: property["label"],
                label: values[key][k][0]["label"],
              });
            }
          }
        });
      });
    })
    .catch((error) => {
      throw new Error(error.reason);
    });

  for (let i = 0; i < relatedPapers.length; i++) {
    let contributions = relatedPapers[i].contributions;
    let t = [];
    contributions.map((c) => {
      let ct = [];
      ct.push(comparisonData[c["resource_id"]]);
      t.push({ resource_id: c["resource_id"], contribution_details: ct[0] });
    });

    res.push({
      resource_id: relatedPapers[i]["resource_id"],
      id: relatedPapers[i]["id"],
      label: relatedPapers[i]["label"],
      totalContributions: relatedPapers[i]["totalContributions"],
      contributions: t,
      authors: relatedPapers[i]["authors"],
    });
  }
  return res;
}

async function filterComparisonResult(contributions, where) {
  let response = [];
  if (contributions && "contributions" in contributions) {
    const results = await Promise.all(
      contributions.contributions.map(async (contribution) => {
        const result = await satisfiesAllConditions(
          contribution.data,
          contribution.askOrkgDetails,
          where
        );
        console.log("result", result);
        return result ? contribution : null;
      })
    );

    response = results.filter((contribution) => contribution !== null);
  }
  if (response.length > 0) {
    //console.log('response', response.length)
    return {
      label: contributions.label,
      doi: contributions.doi,
      id: contributions.id,
      contributions: response,
    };
  }
}

function applyFilter(valueString, condition) {
  // Numeric comparisons: Check if a numeric condition is explicitly stated
  if (
    "_LT" in condition ||
    "_LTE" in condition ||
    "_GT" in condition ||
    "_GTE" in condition ||
    "EQ" in condition ||
    "NEQ" in condition
  ) {
    const value = parseFloat(valueString);
    if (isNaN(value)) {
      return false; // If conversion fails and a numeric comparison is expected, return false
    }

    if (condition._LT !== undefined && !(value < condition._LT)) return false;
    if (condition._LTE !== undefined && !(value <= condition._LTE))
      return false;
    if (condition._GT !== undefined && !(value > condition._GT)) return false;
    if (condition._GTE !== undefined && !(value >= condition._GTE))
      return false;
    if (condition._EQ !== undefined && !(value === condition._EQ)) return false;
    if (condition._NEQ !== undefined && !(value !== condition._NEQ))
      return false;
  }

  if (condition.value !== undefined) {
    const lowerValueString = valueString.toLowerCase();
    const lowerConditionValue = condition.value.toLowerCase();
    // Check if valueString contains condition.value or if condition.value contains valueString
    if (
      !(
        lowerValueString.includes(lowerConditionValue) ||
        lowerConditionValue.includes(lowerValueString)
      )
    ) {
      return false; // If neither string contains the other, return false
    }
  }

  return true; // Return true if none of the conditions are violated
}

async function satisfiesAllConditions(data, askOrkgDetails, where) {
  for (let i = 0; i < where.length; i++) {
    let condition = where[i];
    const item = data.find((d) => d.propertyId === condition.property);
    if (!item) return false;

    if (condition.property === "P5049") {
      //location
      const code = isLocationContinent(condition.value);
      if (code) {
        const country = await findCountriesByContinent(code);
        let cities = [];
        for (let i = 0; i < country.length; i++) {
          if (country[i]["countryCode"] !== undefined) {
            const countryCities = await findCitiesByCountry(
              country[i]["countryCode"]
            );
            cities = cities.concat(
              countryCities.map((city) => ({
                country: country.name,
                city,
              }))
            );
          }
        }
        console.log(
          cities.some(
            (entry) =>
              item.label.includes(entry.city.name) ||
              item.label.includes(entry.country)
          )
        );
        return cities.some(
          (entry) =>
            item.label.includes(entry.city.name) ||
            item.label.includes(entry.country)
        );
      } else {
        console.log(item.label === condition.value);
        return item.label === condition.value;
      }
      // for all other properties
    } else {
      if (condition.property === "METHOD") {
        if (item.label == "" && askOrkgDetails.method !== "") {
          return searchValue(askOrkgDetails.method, condition.value);
        } else {
          return searchValue(item.label, condition.value);
        }
      }
    }
    return false;
  }
}

function searchValue(valueString, conditionValue) {
  if (conditionValue !== undefined) {
    const lowerValueString = valueString.toLowerCase();
    const lowerConditionValue = conditionValue.toLowerCase();
    // Check if valueString contains condition.value or if condition.value contains valueString
    if (
      !(
        lowerValueString.includes(lowerConditionValue) ||
        lowerConditionValue.includes(lowerValueString)
      )
    ) {
      return false; // If neither string contains the other, return false
    }
  }
  return true;
}

function isLocationContinent(continentName) {
  const continentCodes = {
    Asia: "AS",
    Africa: "AF",
    "North America": "NA",
    "South America": "SA",
    Antarctica: "AN",
    Europe: "EU",
    Australia: "OC",
  };

  const normalizedContinentName = continentName.trim();

  return continentCodes[normalizedContinentName] || false;
}

function filterPaperWithCitations(papers, paperCitations, citationCount) {}

module.exports = { getComparisonData, filterComparisonResult };
