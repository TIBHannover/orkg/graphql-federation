const combinedTypeDefs = `type country {
			name: String,
			countryCode: String,
			population: String,
			lng: String,
			lat: String
			city(searchTerm: String): [city]
			Problem(label: String): [Problem]
		}

		type city {
			name: String,
			countryCode: String,
			population: String,
			lng: String,
			lat: String
		}

		type SemanticScholarPapers {
			title: String!
			doi: String!
			year: String!
			authors: [String]!
			abstract: String!
			peerReview(id: String): PeerReview!
			citations: [SemanticScholarPapers]!
			references: [SemanticScholarPapers]!
			Paper(doi: String, field: [String]): Paper
			project(doi: String): OpenAirePapers
			work: Work
			topics: WikidataInformation
		}
		
		type OpenAirePapers {
			title: String!,
			authors: String!,
			year: String!,
			doi: String!,
			funder: String!,
			project: String!
		}
		
		type WikidataInformation {
			title: String
			authors: String
			year: String
			doi: String
			topic: [String]
		}

		type OrkgAsk {
			doi: String
			title: String
			details: paperDetails
		}

		type paperDetails {
			method: String
			results: String
			description: String
		}
		type t {
			doi: String
			title: String
		}

		type Project {
			id: String
			title: String
			papers: [Paper]
		}

		extend type Work {
			comparison(where: [FilterCondition]): Comparison
			Paper(doi: String, field: [String]): Paper
			project(doi: String): OpenAirePapers
			semanticScholarMetadata: SemanticScholarPapers
		}

		extend type Comparison {
			work: Work
			papers: Paper
		}
		extend type Literal {
			work(id: String): Work
		}

		extend type PublicationConnectionWithTotal {
			details: [Paper]
		}

		extend type Publication {
			details: [Paper]
		}

		extend type Paper {
			peerReview(id: String): PeerReview
			findByLocation(location: String): [Paper]
			work(id: String): Work
			citationCount: Int
		}

		extend type ContributionDetail {
  			askORKGdetails: paperDetails
		}

		extend type Problem {
			dissertations(query: String): DissertationConnectionWithTotal
		}
		extend type WorkConnectionWithTotal {
			Paper(doi: String): [Paper]
		}

		extend type Person @key(fields: "id") {
			publicationsByLabel(label: String): [PublicationConnectionWithTotal]
			topics: [String]
			work(where: [FilterCondition]): Author

		}
		
		extend type Funding {
			projectDetails: String
		}

		type Query @extends {
			singleCountry(searchTerm: String): country
			findCountriesByContinent(searchTerm: String): [country]
			findCitiesByCountry(searchTerm: String): [city]
			semanticScholarPapers(searchTerm: String): [SemanticScholarPapers]
			semanticScholarPaper(doi: String): SemanticScholarPapers
			OpenAirePaper(doi: String!): OpenAirePapers
			papersWithSemanticDetails(query: String): [OrkgAsk]
			project(id: String, where: [FilterCondition]): Project
			papers(field: [String], where: [FilterCondition], MinCitationCount: Int): [Paper]
			comparison(doi: String, where: [FilterCondition]): Comparison
		}
`;

module.exports = { combinedTypeDefs };
