var rp = require("request-promise");
const { get, chain } = require("lodash");

function singleCountry(searchTerm) {
  const url = `https://secure.geonames.org/countryInfoJSON?country=${searchTerm}&username=mharis111`;
  const options = {
    uri: url,
    json: true,
  };
  return rp(options)
    .then((data) => {
      const geoNames = get(data, "geonames");
      return geoNames
        .map(
          ({
            capital,
            continent,
            population,
            continentName,
            countryName,
            currencyCode,
            countryCode,
          }) => {
            return {
              continentName,
              name: countryName,
              currencyCode,
              countryCode,
              population,
              continentCode: continent,
              capital,
            };
          }
        )
        .first()
        .value();
    })
    .catch((error) => {
      throw new Error(error.reason);
    });
}

function findCountriesByContinent(searchTerm) {
  const url = `https://secure.geonames.org/search?continentCode=${searchTerm}&type=json&username=mharis111`;
  const options = {
    uri: url,
    json: true,
  };
  return rp(options)
    .then((data) => {
      var geoNames = get(data, "geonames");
      geoNames = [
        ...new Map(
          geoNames.map((item) => [JSON.stringify(item.countryName), item])
        ).values(),
      ];
      return geoNames.map(
        ({ population, countryName, lng, lat, countryCode }) => {
          return {
            name: countryName,
            countryCode,
            population,
            lng,
            lat,
          };
        }
      );
    })
    .catch((error) => {
      throw new Error(error.reason);
    });
}

function findCitiesByCountry(searchTerm) {
  const url = `https://secure.geonames.org/search?country=${searchTerm}&type=json&username=mharis111`;
  const options = {
    uri: url,
    json: true,
  };
  return rp(options)
    .then((data) => {
      var geoNames = get(data, "geonames");
      return geoNames.map(({ population, name, lng, lat, countryCode }) => {
        return {
          name,
          countryCode,
          population,
          lng,
          lat,
        };
      });
    })
    .catch((error) => {
      throw new Error(error.reason);
    });
}

module.exports = {
  singleCountry,
  findCountriesByContinent,
  findCitiesByCountry,
};
