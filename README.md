# ORKG GraphQL

This repository belongs to federated GraphQL endpoint for the Open Research Knowledge Grapph (ORKG) and DataCite. 

### Installation

Clone this repository:

    git clone https://gitlab.com/TIBHannover/orkg/graphql-federation.git

Go to the graphql federation directory:

    cd graphql-federation

Install the dependencies by running:

    npm install

### ORKG GraphQL service

In order to run the federated GraphQL endpoint, the ORKG GraphQL needs to be running as well. Please refer to the [ORKG GraphQL repository](https://gitlab.com/TIBHannover/orkg/orkg-graphql).

## Running

Run the following command:

    npm run start

Open the browser and enter the URL of the application: http://localhost:4001/graphiql.

## Examples

Find a paper and it's citations on DataCite and retrieve the details of comparisons from ORKG which have cited this paper.

{
  work(id: "https://doi.org/10.1101/2020.03.08.20030643") {
    id
    titles {
      title
    }
    citations {
      nodes {
        id
        titles {
          title
        }
        publisher
      }
    }
  }
}
