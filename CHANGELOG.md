# Changelog

All changes related to the federated GraphQL will be documented in this file. The format is based on [Keep a
Changelog](https://keepachangelog.com/en/1.0.0/).

---
## V0.1 - 2021-05-03

### Changes

- Federated endpoint supporting ORKG and DataCite.
