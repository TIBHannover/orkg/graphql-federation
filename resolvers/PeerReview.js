const getDataCiteSchema = require('../DataCite.js');

const PeerReviewResolver = {
  peerReview: {
    async resolve(parent, args, context, info) {
      try {
        if (parent.doi != null) {
          let id = parent.doi;
          let r = await info.mergeInfo.delegateToSchema({
            schema: await getDataCiteSchema(),
            operation: 'query',
            fieldName: 'peerReview',
            args: { id },
            context,
            info,
          });
          return r;
        } else {
          return { type: '' };
        }
      } catch (err) {
        return { type: '' };
      }
    },
  },
};

module.exports = { PeerReviewResolver };
