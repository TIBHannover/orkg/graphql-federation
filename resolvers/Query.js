const {
  singleCountry,
  findCountriesByContinent,
  findCitiesByCountry,
} = require("../GeoNames/geoConnector");

const {
  getPaperFromOpenAire,
  getProjectFromOpenAire,
} = require("../resolvers/OpenAire");
const {
  getPaperFromSemanticScholarByDoi,
} = require("../resolvers/SemanticScholar");
const { getPaperFromOrkgAsk } = require("../resolvers/OrkgAsk");
const { getPapersFromOrkg } = require("../resolvers/Papers");
const { getComparisonFromOrkg } = require("../resolvers/Comparison");

const QueryResolver = {
  async singleCountry(root, { searchTerm }) {
    const country = await singleCountry(searchTerm);
    return country;
  },
  async findCountriesByContinent(root, { searchTerm }) {
    const country = await findCountriesByContinent(searchTerm);
    var filteredArray = country.filter((o1) => o1.name !== undefined);
    return filteredArray;
  },

  async findCitiesByCountry(parent, { searchTerm }) {
    const country = findCitiesByCountry(searchTerm);
    return country;
  },

  async OpenAirePaper(parent, { doi }) {
    const result = await getPaperFromOpenAire(doi);
    return result;
  },

  async semanticScholarPaper(parent, { doi }) {
    const result = await getPaperFromSemanticScholarByDoi(doi);
    return result;
  },

  async papersWithSemanticDetails(parent, { query }) {
    const result = await getPaperFromOrkgAsk(query);
    return result;
  },

  async project(parent, args, context, info) {
    const result = await getProjectFromOpenAire(args);
    return result;
  },

  async papers(parent, args, context, info) {
    const result = await getPapersFromOrkg(parent, args, context, info);
    return result;
  },

  async comparison(parent, args, context, info) {
    const result = await getComparisonFromOrkg(parent, args, context, info);
    return result;
  },
};

module.exports = { QueryResolver };
