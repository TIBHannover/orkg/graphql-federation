const {
  findCitiesByCountry,
} = require('../GeoNames/geoConnector');
const CountryResolver = {
  city: parent => {
    const id = parent.countryCode;
    const country = findCitiesByCountry(id);
    return country;
  },
  Problem: {
    async resolve(parent, args, context, info) {
      try {
        let label = 'Determination of the COVID-19 basic reproduction number';
        return info.mergeInfo.delegate(
          'query',
          'Problem',
          { label },
          context,
          info,
        );
      } catch (err) {
        return { id: 'null' };
      }
    },
  },
};

module.exports = { CountryResolver };
