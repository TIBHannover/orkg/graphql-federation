const getDataCiteSchema = require('../DataCite.js');
const PaperResolver = {
  peerReview: {
    async resolve(parent, args, context, info) {
      try {
        console.log('parent', parent);
        if (parent.doi != null) {
          let id = parent.doi;
          let r = await info.mergeInfo.delegateToSchema({
            schema: await getDataCiteSchema(),
            operation: 'query',
            fieldName: 'peerReview',
            args: { id },
            context,
            info,
          });
          return r;
        } else {
          return { type: 'not found' };
        }
      } catch (err) {
        return { type: 'not found' };
      }
    },
  },
  work: {
    fragment: `fragment PaperFragment on Paper {id{label}}`,
    async resolve(parent, args, context, info) {
      try {
        if (parent.doi) {
          let id = parent.doi;
          let r = await info.mergeInfo.delegateToSchema({
            schema: await getDataCiteSchema(),
            operation: 'query',
            fieldName: 'work',
            args: { id },
            context,
            info,
          });
          if (r == null) r = { id: 'null', citationCount: 0 };
          return r;
        } else {
          return { id: 'null', citationCount: 0 };
        }
      } catch (err) {
        return { id: 'null', citationCount: 0 };
      }
    },
  },
};

module.exports = { PaperResolver };
