const getORKGSchema = require("../ORKG.js");
const getDataCiteSchema = require("../DataCite.js");

const { getAuthorInformationFromWikidata } = require("./Wikidata");

const personResolvers = {
  topics: {
    fragment: `fragment PersonFragment on Person {id}`,
    async resolve(parent, args, context, info) {
      let id = parent.id;
      id = id.replace("https://orcid.org/", "");
      let result = await getAuthorInformationFromWikidata(id);
      return result;
    },
  },

  work: {
    fragment: `fragment PersonFragment on Person {id}`,
    async resolve(parent, args, context, info) {
      let id = parent.id.replace("https://orcid.org/", "");
      const response = await info.mergeInfo.delegateToSchema({
        schema: await getORKGSchema(),
        operation: "query",
        fieldName: "author",
        args: { id: id },
        context,
        info,
      });

      console.log(response);
      return response;
    },
  },
};

module.exports = { personResolvers };
