var rp = require("request-promise");
const { get, chain } = require("lodash");
const PeerReviewResolver = require("./PeerReview");
const getORKGSchema = require("../ORKG.js");
const { getPaperFromOpenAire } = require("./OpenAire");
const getDataCiteSchema = require("../DataCite.js");
const { getPaperInformationFromWikidata } = require("./Wikidata.js");

async function getPaperFromSemanticScholar(searchTerm) {
  const url = `https://api.semanticscholar.org/graph/v1/paper/search?query=${searchTerm}&limit=20&fields=title,authors,externalIds,citationCount,abstract,year`;
  const options = {
    uri: url,
    json: true,
  };

  return rp(options).then((data) => {
    if (data["total"] > 0) {
      const searchResult = get(data, "data");

      return searchResult.map((r) => {
        return {
          title: r["title"] ? r["title"] : "",
          doi:
            r["externalIds"] && r["externalIds"]["DOI"]
              ? r["externalIds"]["DOI"]
              : "",
          authors: r["authors"]
            ? r["authors"].map((ra) => {
                return ra["name"];
              })
            : [""],
          citations: r["citationCount"],
          year: r["year"],
          abstract: r["abstract"] ? r["abstract"] : "",
        };
      });
    } else {
      let res = [];
      res.push({
        title: "",
        doi: "",
        authors: [""],
        citations: 0,
        year: "",
        abstract: "",
      });
      return res;
    }
  });
}

async function getPaperFromSemanticScholarByDoi(id) {
  const url = `https://api.semanticscholar.org/v1/paper/${id}`;
  const options = {
    uri: url,
    json: true,
  };

  return rp(options).then((data) => {
    if (data) {
      return {
        title: data["title"] ? data["title"] : "",
        doi: data["doi"] ? data["doi"] : "",
        authors: data["authors"]
          ? data["authors"].map((ra) => {
              return ra["name"];
            })
          : [""],
        citations: data["citations"]
          ? data["citations"].map((ra) => {
              return {
                doi: ra["doi"] ? ra["doi"] : "",
                title: ra["title"] ? ra["title"] : "",
                year: ra["year"] ? ra["year"] : "",
              };
            })
          : [""],
        year: data["year"],
        abstract: data["abstract"] ? data["abstract"] : "",
        references: data["references"]
          ? data["references"].map((ra) => {
              return {
                doi: ra["doi"] ? ra["doi"] : "",
                title: ra["title"] ? ra["title"] : "",
                year: ra["year"] ? ra["year"] : "",
              };
            })
          : [""],
      };
    } else {
      let res = [];
      res.push({
        title: "",
        doi: "",
        authors: [""],
        citations: 0,
        year: "",
        abstract: "",
        references: 0,
      });
      return res;
    }
  });
}

async function fetchPaperDetails(ids) {
  const options = {
    method: "POST",
    uri: "https://api.semanticscholar.org/graph/v1/paper/batch",
    qs: {
      fields: "referenceCount,citationCount,title,externalIds",
    },
    body: {
      ids: ids,
    },
    json: true,
  };

  try {
    const response = await rp(options);
    console.log(response);
    return response;
  } catch (error) {
    console.error("Error fetching data:", error.error ? error.error : error);
  }
}

const SemanticScholarResolvers = {
  Paper: {
    fragment: `fragment SemanticscholarFragment on SemanticScholarPapers {doi}`,
    async resolve(parent, args, context, info) {
      console.log(parent.doi);
      let doi = parent.doi;
      doi = doi.replace("https://doi.org/", "");
      let r = info.mergeInfo.delegateToSchema({
        schema: await getORKGSchema(),
        operation: "query",
        fieldName: "Paper",
        args: { doi },
        context,
        info,
      });
      return r;
    },
  },

  project: {
    fragment: `fragment SemanticscholarFragment on SemanticScholarPapers {doi}`,
    async resolve(parent, args, context, info) {
      let doi = parent.doi;
      doi = doi.replace("https://doi.org/", "");
      const result = await getPaperFromOpenAire(doi);
      return result;
    },
  },

  work: {
    fragment: `fragment SemanticscholarFragment on SemanticScholarPapers {doi}`,
    async resolve(parent, args, context, info) {
      try {
        if (parent.doi) {
          let id = parent.doi;
          let r = await info.mergeInfo.delegateToSchema({
            schema: await getDataCiteSchema(),
            operation: "query",
            fieldName: "work",
            args: { id },
            context,
            info,
          });
          if (r == null) r = { id: "null", citationCount: 0 };
          return r;
        } else {
          return { id: "null", citationCount: 0 };
        }
      } catch (err) {
        return { id: "null", citationCount: 0 };
      }
    },
  },

  topics: {
    fragment: `fragment SemanticscholarFragment on SemanticScholarPapers {doi}`,
    async resolve(parent, args, context, info) {
      let doi = parent.doi;
      doi = doi.replace("https://doi.org/", "");
      const result = await getPaperInformationFromWikidata(doi);
      return result;
    },
  },
};

module.exports = {
  getPaperFromSemanticScholar,
  getPaperFromSemanticScholarByDoi,
  SemanticScholarResolvers,
  fetchPaperDetails,
};
