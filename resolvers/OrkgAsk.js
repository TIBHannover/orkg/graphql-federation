var rp = require("request-promise");
const { get, chain } = require("lodash");
const fs = require("fs");
const csv = require("csvtojson");

const doiToConsumerIdMappings = [
  { doi: "10.1101/2020.03.03.20029983", consumerId: "1kgnejhv3w63hlp" },
  { doi: "10.25561/77148", consumerId: "l1ixwk3e1572q2u" },
  { doi: "10.1101/2020.01.23.917351", consumerId: "hhfrmn70ayz5iyd" },
  { doi: "10.1056/nejmoa2001316", consumerId: "hygudsd43ajka3x" },
  { doi: "10.1101/2020.01.23.917351", consumerId: "5g7bdcegv4xeynb" },
  { doi: "10.1101/2020.03.08.20030643", consumerId: "5g7bdcegv4xeynb" },
  { doi: "10.1101/2020.03.05.20031815", consumerId: "5g7bdcegv4xeynb" },
  { doi: "10.3390/jcm9020388", consumerId: "5g7bdcegv4xeynb" },
  { doi: "10.3390/jcm9020462", consumerId: "5g7bdcegv4xeynb" },
  { doi: "10.1101/2020.02.18.20024315", consumerId: "5g7bdcegv4xeynb" },
  { doi: "10.1101/2020.03.05.20031815", consumerId: "5g7bdcegv4xeynb" },
  { doi: "10.1101/2020.03.19.20037945", consumerId: "5g7bdcegv4xeynb" },
  { doi: "10.1016/j.ijid.2020.02.033", consumerId: "5g7bdcegv4xeynb" },
  { doi: "10.2139/ssrn.3525558", consumerId: "5g7bdcegv4xeynb" },
  { doi: "10.1101/2020.01.23.916726", consumerId: "5g7bdcegv4xeynb" },
  { doi: "10.1016/j.ijid.2020.01.050", consumerId: "5g7bdcegv4xeynb" },
  { doi: "10.1016/s0140-6736(20)30260-9", consumerId: "5g7bdcegv4xeynb" },
  { doi: "10.2139/ssrn.3524675", consumerId: "5g7bdcegv4xeynb" },
  { doi: "10.1101/2020.01.23.20018549", consumerId: "5g7bdcegv4xeynb" },
  { doi: "10.1101/2020.01.25.919787", consumerId: "5g7bdcegv4xeynb" },
  { doi: "10.3390/jcm9020523", consumerId: "5g7bdcegv4xeynb" },
  { doi: "10.1056/nejmoa2001316", consumerId: "5g7bdcegv4xeynb" },
  { doi: "10.1101/2020.01.23.917351", consumerId: "5g7bdcegv4xeynb" },
  { doi: "10.1016/j.ijid.2020.02.033", consumerId: "1kgnejhv3w63hlp" },
];

function getPaperID(doi) {
  const mapping = doiToConsumerIdMappings.find(
    (mapping) => mapping.doi === doi
  );

  // Return the consumerId if a match is found, otherwise return null
  return mapping ? mapping.consumerId : null;
}

async function getPaperFromOrkgAsk(doi) {
  reults = [];
  //console.log('doi', getPaperID(doi));
  const paperId = getPaperID(doi);
  //const url = `https://api.ask.orkg.org/index/explore?filter=doi IN [${query}]&limit=1`;
  if (paperId == null) {
    return {
      method: "",
      results: "",
      description: "",
    };
  }
  const url = `https://api.ask.orkg.org/llm/extract/item/values?properties=method&properties=Insights&properties=TL%3BDR&properties=Conclusions&properties=Results&properties=Methods&properties=Limitations&collection_item_id=${getPaperID(
    doi
  )}`;
  //console.log(url);
  const options = {
    uri: url,
    headers: {
      "User-Agent": "Request-Promise",
    },
    json: true, // Automatically parses the JSON string in the response
  };

  return rp(options)
    .then(function (response) {
      const { uuid, timestamp, payload } = response;
      const { item_id, properties, values, extra } = payload;
      let method = "";
      //console.log(values.Methods.join(", "));

      return {
        method: values.Methods.join(", "),
        results: "",
        description: values["TL;DR"].join(", "),
      };
    })
    .catch(function (err) {
      console.error(`Failed to retrieve data: ${err.message}`);
    });
}

async function getPapersFromOrkgAsk(query) {
  reults = [];

  const url = `https://api.ask.orkg.org/index/search?query=${query}&limit=5&offset=0`;

  // Specify your query
  const options = {
    uri: url,
    headers: {
      "User-Agent": "Request-Promise",
    },
    json: true, // Automatically parses the JSON string in the response
  };

  // Send a GET request to the API
  return rp(options)
    .then(function (response) {
      //console.log(response.payload.items)
      return response.payload.items.map((item) => ({
        doi: item.doi, // Replace 'item.doi' with actual path to DOI in the response
        title: item.title, // Replace 'item.title' with actual path to title in the response
        details: {
          method: item.method, // Replace 'item.method' with actual path to method in the response
          results: item.results, // Replace 'item.results' with actual path to results in the response
          description: item.description, // Replace 'item.description' with actual path to description in the response
        },
      }));
    })
    .catch(function (err) {
      console.error(`Failed to retrieve data: ${err.message}`);
      return []; // Return an empty array in case of an error
    });
}

module.exports = { getPaperFromOrkgAsk };
