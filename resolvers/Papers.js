var rp= require('request-promise');
const { get, chain }= require('lodash');
const fs = require('fs');
const csv=require('csvtojson');
const getORKGSchema = require('../ORKG.js');
const { getPaperFromSemanticScholarByDoi, fetchPaperDetails } = require('./SemanticScholar.js');

async function getPapersFromOrkg(parent, args, context, info) {
    let where = undefined;
    let field = undefined;
    let citationCount = -1;
        if (args.where && args.where.length > 0) {
            where =  args.where;
        }

        if (args.field && args.field.length > 0) {
            field =  args.field;
        }

        if (args.MinCitationCount) {
            citationCount = args.MinCitationCount;
        }

      const orkgSchema = await getORKGSchema();
      let papersDetails = await info.mergeInfo.delegateToSchema({
          schema: orkgSchema,
          operation: 'query',
          fieldName: 'papers', // This should match the query name in the ORKG schema
          args: {
            field: field, where: where
          },
          context,
          info,
        });

      if (args.MinCitationCount && citationCount!= -1) {
        const dois = papersDetails.map(p => p.doi);
        let paperData = await fetchPaperDetails(dois);
        paperData = paperData.filter(p => p !== null);
        const citationsMap = new Map(paperData.map(p => [p.externalIds.DOI, p.citationCount]));
        console.log(citationsMap);
        papersDetails = papersDetails.filter(p => citationsMap.get(p.doi) > citationCount);
        //paperDetails = filteredPapers;
        papersDetails = papersDetails.map(paper => {
        const citationCount = citationsMap.get(paper.doi) || 0; // Default to 0 if no data is found
        return { ...paper, citationCount }; // Spread the existing paper details and add the citationCount
    })
        console.log(papersDetails);

      }
      console.log("----");
      console.log(papersDetails);
      //papersDetails = papersDetails.filter(paper => paper.id !== null || paper.label !== null);

      //return parent.papers;
      return papersDetails
}

module.exports= { getPapersFromOrkg };
