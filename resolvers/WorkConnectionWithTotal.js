const getORKGSchema = require('../ORKG.js');

const WorkConnectionWithTotalResolver = {
  Paper: {
    async resolve(parent, args, context, info) {
      try {
        let doi = parent.nodes[0].id;
        doi = doi.replace('https://doi.org/', '');
        return info.mergeInfo.delegateToSchema({
          schema: await getORKGSchema(),
          operation: 'query',
          fieldName: 'Paper',
          args: { doi },
          context,
          info
        });
      } catch (err) {
        return { id: 'null' };
      }
    },
  },
};

module.exports = { WorkConnectionWithTotalResolver };
