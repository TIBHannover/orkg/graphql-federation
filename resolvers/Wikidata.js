var rp= require('request-promise');
const { get, chain }= require('lodash');
const { getPaperFromOpenAire } = require('./OpenAire');
const fetch = require("node-fetch");

async function getPaperInformationFromWikidata(doi) {
   //const url = `https://www.wikidata.org/w/api.php`;
   const url = `https://www.wikidata.org/w/api.php?srsearch=${doi}&action=query&format=json&prop=info&list=search`;
   const options = {
     uri: url,
     json: true
   };

   return rp(options).then(async (data)=> {
   let idList = [];
   	if(data['query']['search']) {
   	console.log(data['query']['search'][0]['title']);
   	const propertyUrl = `https://www.wikidata.org/w/api.php?ids=${data['query']['search'][0]['title']}&action=wbgetentities&format=json`;
   	const o = {
     		uri: propertyUrl,
     		json: true
   	};
   	
   	await rp(o).then((response) => {
   		//console.log(response['entities'][data['query']['search'][0]['title']]);
   		if(response['entities'][data['query']['search'][0]['title']]['claims']['P921']) {
   			let properties = response['entities'][data['query']['search'][0]['title']]['claims']['P921'];
   			for(let i=0; i< properties.length; i++) {
   				idList.push(properties[i]['mainsnak']['datavalue']['value']['id']);
   			}
   		}
   	});
   	
   	console.log(idList);
   	//------------------------------------------------
   	const entityUrl = `https://www.wikidata.org/w/api.php?ids=${idList.join("|")}&action=wbgetentities&format=json`;
   	const e = {
     		uri: entityUrl,
     		json: true
   	};
   	let topics = [];
   	await rp(e).then((response) => {
   		//console.log(response);
   		let k = Object.keys(response['entities']);
   		
   		for(let i=0; i<k.length; i++) {
   			//console.log(response['entities'][k[i]]['labels']['en']['value']);
   			topics.push(response['entities'][k[i]]['labels']['en']['value']);
   		}
   		
   	});
   	
   	
   	return {
   		title: "",
		authors: "",
		year: "",
		doi: "",
		topic: topics
   		};
   	}
   	return {
   	title: "",
	authors: "",
	year: "",
	doi: "",
	topic: []
   	};
   });
}

async function getAuthorInformationFromWikidata(id) {

const url = `https://www.wikidata.org/w/api.php?srsearch=${id}&action=query&format=json&prop=info&list=search`;
   const options = {
     uri: url,
     json: true
   };
   
   let qId = await rp(options).then(async (data)=> {
   	if(data['query']['search'] && data['query']['search'].length > 0) {
   		let res = data['query']['search'];
   		let qId= '';
   		for(let i=0; i<res.length; i++) {
   			if(res[i]['snippet']==='researcher') {
   				//console.log(res[i]['title']);
   				qId=res[i]['title'];
   			}
		}
		return qId;
   	}
   });
   console.log(qId);
   
if(qId!=='') {

const endpointUrl = 'https://query.wikidata.org/sparql';
const sparqlQuery = `SELECT ?score ?topic ?topicLabel
WITH {
  SELECT
    (SUM(?score_) AS ?score)
    ?topic
  WHERE {
    { 
      wd:${qId} wdt:P101 ?topic .
      BIND(20 AS ?score_)
    }
    UNION
    {
      SELECT (3 AS ?score_) ?topic WHERE {
        ?work wdt:P50 wd:Q56755378 ;
              wdt:P921 ?topic . 
      }
    }
    UNION
    {
      SELECT (1 AS ?score_) ?topic WHERE {
        ?work wdt:P50 wd:Q56755378 .
        ?citing_work wdt:P2860 ?work .
        ?citing_work wdt:P921 ?topic . 
      }
    }
  }
  GROUP BY ?topic
} AS %results 
WHERE {
  INCLUDE %results
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en,da,de,es,jp,no,ru,sv,zh". }
}
ORDER BY DESC(?score)
LIMIT 200`;

const fullUrl = endpointUrl + '?query=' + encodeURIComponent( sparqlQuery );
		const headers = { 'Accept': 'application/sparql-results+json' };
		//const options = {
     			//uri: fullUrl,
     			//headers
   		//};
   		
   		//rp(options).then(async (data)=> {
   			//console.log(data);
   		//});

		 return fetch( fullUrl, { headers } ).then( body => body.json() )
		 	.then((data) => {
		 		if(data['results']['bindings']) {
		 		let arr = [];
		 		let res = data['results']['bindings'];
		 			for(let i=0; i<res.length; i++) {
		 				console.log(res[i]['topicLabel']['value']);
		 				arr.push(res[i]['topicLabel']['value']);
		 			}
		 			return arr;
		 		}
		 		});
		 		
	}
	return [""];

	
}

module.exports= {getPaperInformationFromWikidata, getAuthorInformationFromWikidata};
