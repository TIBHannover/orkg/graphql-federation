const getDataCiteSchema = require('../DataCite.js');

const LiteralResolver = {
  work: {
    async resolve(parent, args, context, info) {
      try {
        if (parent.label) {
          let id = parent.label;
          let r = await info.mergeInfo.delegateToSchema({
            schema: await getDataCiteSchema(),
            operation: 'query',
            fieldName: 'work',
            args: { id },
            context,
            info,
          });
          if (r == null) r = { id: 'null', citationCount: 0 };
          return r;
        } else {
          return { id: 'null', citationCount: 0 };
        }
      } catch (err) {
        return { id: 'null', citationCount: 0 };
      }
    },
  },
};

module.exports = { LiteralResolver };
