const getDataCiteSchema = require('../DataCite.js');
const ProblemResolver = {
  dissertations: {
    async resolve(parent, args, context, info) {
      try {
        if (parent.label != null) {
          let query = parent.label;
          let r = await info.mergeInfo.delegateToSchema({
            schema: await getDataCiteSchema(),
            operation: 'query',
            fieldName: 'dissertations',
            args: { query },
            context,
            info,
          });
          return r;
        } else {
          return { type: 'not found' };
        }
      } catch (err) {
        return { type: 'not found' };
      }
    },
  },
};

module.exports = { ProblemResolver };
