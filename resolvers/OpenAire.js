var rp = require("request-promise");
const { get, chain } = require("lodash");
const fs = require("fs");
const csv = require("csvtojson");
const getORKGSchema = require("../ORKG.js");

async function getPaperFromOpenAire(searchTerm) {
  const url = `http://api.openaire.eu/search/publications/?doi=${searchTerm}&format=csv`;
  const options = {
    uri: url,
    csv: true,
  };

  return rp(options).then((data) => {
    return csv({
      noheader: true,
      output: "csv",
    })
      .fromString(data)
      .then((csvRow) => {
        return {
          title: csvRow[1][0] ? csvRow[1][0] : "",
          authors: csvRow[1][1] ? csvRow[1][1] : [""],
          year: csvRow[1][2] ? csvRow[1][2] : "",
          doi: csvRow[1][3] ? csvRow[1][3] : "",
          funder: csvRow[1][7] ? csvRow[1][7] : "",
          project: csvRow[1][8] ? csvRow[1][8] : "",
        };
      });
  });
}

async function getProjectFromOpenAire(args) {
  const id = args.id;
  const url = `https://api.openaire.eu/search/projects?grantID=${encodeURIComponent(
    id
  )}&format=json`;

  const req_header = {
    uri: url,
    json: true,
  };

  const project_info = await rp(req_header);
  //console.log(project_info.response.results.result[0].metadata['oaf:entity']['oaf:project'].title['$']);
  const project_metadata = project_info.response.results.result[0];
  const acronym =
    project_metadata.metadata["oaf:entity"]["oaf:project"]["acronym"]["$"];
  let title =
    project_metadata.metadata["oaf:entity"]["oaf:project"]["title"]["$"];

  title = acronym + ": " + title;

  const baseUrl = "https://api.openaire.eu/search/publications";
  let currentPage = 1;
  let hasMorePages = true;
  const publicationDetails = [];

  while (hasMorePages) {
    const url = `${baseUrl}?projectID=${encodeURIComponent(
      id
    )}&format=json&page=${currentPage}`;

    try {
      const options = {
        uri: url,
        json: true, // Automatically parses the JSON string in the response
      };

      const response = await rp(options);
      const results = response.response.results.result;
      if (results && results.length > 0) {
        results.map((item) => {
          const detail = {
            title:
              item.metadata["oaf:entity"]["oaf:result"]["title"][0] ||
              "No title available",
            doi:
              item.metadata["oaf:entity"]["oaf:result"]["pid"] ||
              "No DOI available",
            //authors: item.metadata['oaf:entity']['oaf:result']['children']['authors'].map(author => author['fullname']) || []
          };
          publicationDetails.push(detail);
        });
        currentPage++; // Increment to request the next page
      } else {
        hasMorePages = false; // No more pages to fetch
      }
    } catch (error) {
      console.error("Error fetching data from OpenAIRE:", error);
      hasMorePages = false; // Stop the loop in case of an error
    }
  }

  const response = [];
  for (let i = 0; i < publicationDetails.length; i++) {
    response.push({
      label: publicationDetails[i].title["$"],
      id: publicationDetails[i].doi[0]["$"],
    });
  }

  return { id: id, title: title, papers: response, args: args };
}

const OpenAireResolvers = {
  papers: {
    //fragment: `fragment OpenAireFragment on Project { where }`,
    async resolve(parent, args, context, info) {
      let where = undefined;
      if (parent.args.where && parent.args.where.length > 0) {
        where = parent.args.where;
      }

      const orkgSchema = await getORKGSchema();
      const dois = parent.papers
        .map((p) => p.id)
        .filter((id) => id !== undefined && id !== null);
      let papersDetails = await info.mergeInfo.delegateToSchema({
        schema: orkgSchema,
        operation: "query",
        fieldName: "papers", // This should match the query name in the ORKG schema
        args: {
          dois: dois,
          where: where,
        },
        context,
        info,
      });

      //console.log(paperDetailsPromises);
      //papersDetails = papersDetails.filter(paper => paper.id !== null || paper.label !== null);

      //return parent.papers;
      return papersDetails;
    },
  },
};

module.exports = {
  getPaperFromOpenAire,
  getProjectFromOpenAire,
  OpenAireResolvers,
};
