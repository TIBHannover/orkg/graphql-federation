var rp = require("request-promise");
const { get, chain } = require("lodash");
const PeerReviewResolver = require("./PeerReview");
const getORKGSchema = require("../ORKG.js");
const { getPaperFromOpenAire } = require("./OpenAire");
const getDataCiteSchema = require("../DataCite.js");
const { getPaperFromOrkgAsk } = require("./OrkgAsk.js");

const ContributionDetailResolvers = {
  askORKGdetails: {
    fragment: `fragment ContributiondetailFragment on ContributionDetail {paper{id}}`,
    async resolve(parent, args, context, info) {
      let result = { method: "", results: "", description: "" };
      console.log(parent.paper.doi);
      let doi = parent.paper.doi;
      if (doi != null) {
        doi = doi.replace("https://doi.org/", "");
        result = await getPaperFromOrkgAsk(doi);
      }
      return result;
    },
  },
};

module.exports = { ContributionDetailResolvers };
