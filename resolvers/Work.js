const getORKGSchema = require("../ORKG.js");
const { getPaperFromOpenAire } = require("./OpenAire");
const { getPaperFromSemanticScholarByDoi } = require("./SemanticScholar");
const { filterComparisonResult } = require("..//utils/utils.js");

const workResolvers = {
  comparison: {
    fragment: `fragment WorkFragment on Work {id}`,
    async resolve(parent, args, context, info) {
      let doi = parent.id;
      doi = doi.replace("https://doi.org/", "");
      let result = await info.mergeInfo.delegateToSchema({
        schema: await getORKGSchema(),
        operation: "query",
        fieldName: "comparison",
        args: { doi },
        context,
        info,
      });

      if (args.where && args.where !== "") {
        res = await filterComparisonResult(result, args.where); // Note the use of await here
        return res;
      }
      return result; // Return the original result if no filtering is needed
    },
  },

  Paper: {
    fragment: `fragment WorkFragment on Work {id}`,
    async resolve(parent, args, context, info) {
      let doi = parent.id;
      doi = doi.replace("https://doi.org/", "");
      let r = info.mergeInfo.delegateToSchema({
        schema: await getORKGSchema(),
        operation: "query",
        fieldName: "Paper",
        args: { doi },
        context,
        info,
      });
      if (args.field) {
        Promise.all([r]).then((r1) => {
          if (r1[0] != null) {
            var filteredArray = args.field.filter((o1) =>
              r1[0].label.toLowerCase().search(o1)
            );
            return r;
          }
        });
      }
      return r;
    },
  },

  project: {
    fragment: `fragment WorkFragment on Work {id}`,
    async resolve(parent, args, context, info) {
      let doi = parent.id;
      doi = doi.replace("https://doi.org/", "");
      const result = await getPaperFromOpenAire(doi);
      return result;
    },
  },

  semanticScholarMetadata: {
    fragment: `fragment WorkFragment on Work {id}`,
    async resolve(parent, args, context, info) {
      let doi = parent.id;
      doi = doi.replace("https://doi.org/", "");
      const result = await getPaperFromSemanticScholarByDoi(doi);
      return result;
    },
  },
};

module.exports = { workResolvers };
