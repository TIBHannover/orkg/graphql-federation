const { workResolvers } = require('./Work');
const { WorkConnectionWithTotalResolver } = require('./WorkConnectionWithTotal');
const { LiteralResolver } = require('./Literal');
const { PaperResolver } = require('./Paper');
const { ProblemResolver } = require('./Problem');
const { ComparisonResolver } = require('./Comparison');
const { QueryResolver } = require('./Query');
const { CountryResolver } = require('./country');
const { PeerReviewResolver } = require('./PeerReview');
const { SemanticScholarResolvers } = require('./SemanticScholar');
const { personResolvers } = require('./Person');
const { ContributionDetailResolvers } = require('./ContributionDetail');
const { OpenAireResolvers } = require('./OpenAire');

const combinedResolvers = ({
  Work: workResolvers,
  WorkConnectionWithTotal: WorkConnectionWithTotalResolver,
  Literal: LiteralResolver,
  Paper: PaperResolver,
  Problem: ProblemResolver,
  Comparison: ComparisonResolver,
  Query: QueryResolver,
  country: CountryResolver,
  SemanticScholarPapers: SemanticScholarResolvers,
  Person: personResolvers,
  ContributionDetail: ContributionDetailResolvers,
  Project: OpenAireResolvers,
});

module.exports = { combinedResolvers };
