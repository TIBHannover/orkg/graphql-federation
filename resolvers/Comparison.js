/*const {
  findCountriesByContinent, findCitiesByCountry
} = require("../GeoNames/geoConnector");
const ComparisonResolver = {
  async relatedPapers(parent, args, context, info) {
    if (args.property && args.value) {
      const country = await findCountriesByContinent(args.value);
      let cities = [];
      for(let i=0; i<country.length; i++) {
        if(country[i]['countryCode']!==undefined) {
          const c = await findCitiesByCountry(country[i]['countryCode']);
          cities.push({
            key: country[i]['name'],
            data: c
          });
        }
      }
      let res = [];
      for (let i = 0; i < parent.relatedPapers.length; i++) {
        let cont = parent.relatedPapers[i].contributions[0];
        if (cont) {
          for (let j = 0; j < cont.contribution_details.length; j++) {
            if (
              cont.contribution_details[j].property.label
                .toLowerCase()
                .includes(args.property)
            ) {
              for (let k = 0; k < cities.length; k++) {
                if (
                  cont.contribution_details[j].label.toLowerCase().includes(cities[k].key.toLowerCase())
                ) {
                  res.push(parent.relatedPapers[i]);
                } else {
                  let v = cities[k].data.filter(o => o.name===cont.contribution_details[j].label);
                  if( v.length && v.length>0) {
                    let v = cities[k].data.filter(o => o.name===cont.contribution_details[j].label);
                    res.push(parent.relatedPapers[i]);
                  }
                }
              }
            }
          }
        }
      }
      return res;
    } else {
      return parent.relatedPapers;
    }
  },
};

module.exports = { ComparisonResolver };*/

const {
  findCountriesByContinent,
  findCitiesByCountry,
} = require("../GeoNames/geoConnector");
const { getComparisonData } = require("../utils/utils");
var rp = require("request-promise");
const getORKGSchema = require("../ORKG.js");
const { filterComparisonResult } = require("..//utils/utils.js");

async function getComparisonFromOrkg(parent, args, context, info) {
  let results = await info.mergeInfo.delegateToSchema({
    schema: await getORKGSchema(),
    operation: "query",
    fieldName: "comparison",
    args: { doi: args.doi },
    context,
    info,
  });

  let askOrkgDetails = {
    method: "Specific inquiry details here",
  };
  console.log(results);
  results.contributions = results.contributions.map((result) => ({
    id: result.id,
    label: result.label,
    askOrkgDetails,
    paper: result.paper,
    data: result.data,
  }));

  if (args.where && args.where !== "") {
    res = await filterComparisonResult(results, args.where); // Note the use of await here
    return res;
  }
  return results;
}

const ComparisonResolver = {
  async papers(parent, args, context, info) {
    if (args.property && args.value) {
      let comparisonData = await getComparisonData(parent.relatedPapers);

      const country = await findCountriesByContinent(args.value);
      let cities = [];
      for (let i = 0; i < country.length; i++) {
        if (country[i]["countryCode"] !== undefined) {
          const c = await findCitiesByCountry(country[i]["countryCode"]);
          cities.push({
            key: country[i]["name"],
            data: c,
          });
        }
      }

      temp = [];
      comparisonData.map((c) => {
        c["contributions"][0]["contribution_details"].map((r) => {
          //console.log(r['property'])
          if (r["property"] === args.property) {
            //console.log(r['label']);
            for (let k = 0; k < cities.length; k++) {
              if (r["label"].toLowerCase() === cities[k].key.toLowerCase()) {
                temp.push(c);
              } else {
                let v = cities[k].data.filter((o) => o.name === r["label"]);
                if (v.length && v.length > 0) {
                  //let v = cities[k].data.filter(o => o.name===r['label']);
                  temp.push(c);
                }
              }
            }
          }
        });
      });
      return temp;
      /* let res = [];
      let resource_ids = [];
      let ids = '';
      let comparisonData = [];
      console.log(parent.relatedPapers[0]['authors']);
      for (let i = 0; i < parent.relatedPapers.length; i++) {
        let contributions = parent.relatedPapers[i].contributions;

        contributions.map(c => {
          resource_ids.push(c['resource_id']);
        });
      }
        const url=`https://www.orkg.org/orkg/simcomp/compare/?contributions=${resource_ids.join()}&response_hash=8189e27e4d9c597f2784d033f1ae0437&save_response=false&type=merge`;
        const options = {
          uri: url,
          json: true
        };

        await rp(options).then((data) => {
          let comparisonContributions = data['contributions'];
          let properties = data['properties'];
          let values = data['data'];
          let cont = [];
          comparisonContributions.map( c=> {
            comparisonData[c['id']]=[];
            Object.keys(values).map(function(key, index) {
              for(let k=0; k<values[key].length; k++) {
                if(values[key][k][0]['path'] && values[key][k][0]['path'].indexOf(c['id']) !== -1) {
                  let property = properties.find(p => p['id']===key);
                  comparisonData[c['id']].push({property: property['label'], label: values[key][k][0]['label']});
                }
              }
          });
        });
        }).catch((error) => {
          throw new Error(error.reason);
        });
      for (let i = 0; i < parent.relatedPapers.length; i++) {
        let contributions = parent.relatedPapers[i].contributions;
        let t = [];
        contributions.map(c => {
          let ct = [];
          ct.push(comparisonData[c['resource_id']]);
          t.push({ resource_id: c['resource_id'], contribution_details: ct[0]});
        });

        res.push({
          resource_id: parent.relatedPapers[i]['resource_id'],
          id: parent.relatedPapers[i]['id'],
          label: parent.relatedPapers[i]['label'],
          totalContributions:parent.relatedPapers[i]['totalContributions'],
          contributions: t,
          authors: parent.relatedPapers[i]['authors']
      });
    }
      return res;*/
    } else {
      return await getComparisonData(parent.relatedPapers);
    }
  },
};

module.exports = { ComparisonResolver, getComparisonFromOrkg };
